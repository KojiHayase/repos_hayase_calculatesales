package jp.alhinc.hayase_koji.calculate_sales;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CalculateSales {
	public static void main(String[] args) {
		//引数チェック処理
		if(args.length != 1) {
			//引数が予期している状態でないため、エラーメッセージを表示し処理終了
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//全体で利用する変数宣言
		HashMap<String,String> officeMap = new HashMap<String,String>();			//支店情報Map
		HashMap<String,Long> officeSalesSum = new HashMap<String,Long>();			//支店ごとの売上合計Map
		BufferedReader br = null;

		//============================================
		//処理1.支店定義ファイル読み込み
		//============================================
		officeMap = inputMasterFile(args[0] , "branch.lst");

		//返り値がnullの場合は処理終了
		if(officeMap == null) {
			return;
		}else {
			//初期値設定
			officeSalesSum = calcMasterDefaultSet(officeMap.keySet());
		}

		//============================================
		//処理2.集計
		//============================================

		try {
			//処理2-1.コマンドライン引数で指定されたディレクトリから拡張子がrcd、且つファイル名が数字8桁のファイルを検索し、該当ファイルに2-2処理を実行する
			File calcDir = new File(args[0]);
			File[] calcFileList = calcDir.listFiles();

			//ファイルリスト生成に必要な変数宣言
			ArrayList<String> strMatchFileList = new ArrayList<String>();

			//正規表現に合致する(拡張子rcd&&数字8桁)ファイルを検索
			Pattern pattern = Pattern.compile("^.*[0-9]{8}.rcd$");
			Matcher matcher;
			for(int i = 0; i< calcFileList.length; i++) {
				matcher = pattern.matcher(calcFileList[i].getName());
				//チェック処理
				if(matcher.find() && (calcFileList[i].isFile())) {
					strMatchFileList.add(matcher.group(0));
				}
			}

			//ファイル名が昇順になるようにソート
			Collections.sort(strMatchFileList);

			//ファイル読み込みに必要な変数宣言
			Integer prevFileNum = 0;		//ファイル連番チェック用退避変数(前回値)
			Integer nowFileNum = 0;		//ファイル連番チェック用退避変数(現在値)
			Boolean defaultFlg = false;	//初回時のみ連番初期値を設定

			//エラー処理2-1 売上ファイルが連番になっていない場合は、エラーメッセージを表示
			for(String chkFileName : strMatchFileList) {

				//現在値を設定
				nowFileNum = Integer.parseInt(chkFileName.replace(".rcd", ""));

				//初回時のみ比較処理をパス
				if(!defaultFlg) {
					defaultFlg = true;
					prevFileNum = Integer.parseInt(chkFileName.replace(".rcd",""));
					continue;
				}

				if(!((nowFileNum - prevFileNum) == 1)) {
					//エラーメッセージを表示し処理終了
					System.out.println("売上ファイル名が連番になっていません");
					return;
				}

				//前回値として設定
				prevFileNum = Integer.parseInt(chkFileName.replace(".rcd",""));
			}

			//処理2-2.売上げファイルを読み込み、支店コード、売上額を抽出する。
			//		抽出した売上額を該当する支店の合計金額にそれぞれ加算する。それを売上ファイルの数だけ繰り返す。

			//ファイル内容を読み込み,売上履歴リストに退避
			ArrayList<ArrayList<String>> salesHistoryList = new ArrayList<ArrayList<String>>();

			for(String increFileName : strMatchFileList) {
				FileReader fr = new FileReader(args[0] + "\\" + increFileName);
				br = new BufferedReader(fr);
				String content = "";

				ArrayList<String> fileContentList = new ArrayList<String>();

				while((content = br.readLine()) != null){
					fileContentList.add(content);
				}

				//エラー処理2-4 売上ファイルの中身が2行でない場合はエラーメッセージを表示
				if(fileContentList.size() != 2) {
					//エラーメッセージを表示し処理終了
					System.out.println(increFileName + "のフォーマットが不正です");
					return;
				}

				//エラー処理2-3 支店に該当がなかった場合は、エラーメッセージを表示
				if(!officeMap.containsKey(fileContentList.get(0))) {
					//エラーメッセージを表示し処理終了
					System.out.println(increFileName + "の支店コードが不正です");
					return;
				}

				//エラー処理 売上金額が数値かどうか
				if(!(fileContentList.get(1).matches("^[1-9]?[0-9]+$"))) {
					//エラーメッセージを表示し処理終了
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				salesHistoryList.add(fileContentList);
			}

			//ArrayListを用いて集計処理
			for(ArrayList<String> salesHistory : salesHistoryList) {
				//エラー処理2-2 合計金額が10桁を超えた場合、エラーメッセージを表示
				if( officeSalesSum.get(salesHistory.get(0)) + Long.parseLong(salesHistory.get(1)) >= 10000000000L) {
					//エラーメッセージを表示し処理終了
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//合計金額加算処理
				officeSalesSum.put(salesHistory.get(0) , officeSalesSum.get(salesHistory.get(0)) + Long.parseLong(salesHistory.get(1)) );

			}

		}catch(IOException e) {
			//処理2(集計処理中にIOExceptionが発生)
			//エラーメッセージを表示し処理終了
			System.out.println("予期せぬエラーが発生しました");
		}catch(NumberFormatException e){
			//処理2(集計処理中にNumberFormatExceptionが発生)
			//エラーメッセージを表示し処理終了
			System.out.println("予期せぬエラーが発生しました");
		}finally {
			try {
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				//finally処理で正常にcloseできなかった場合
				//エラーメッセージを表示し処理終了
				System.out.println("予期せぬエラーが発生しました");
			}
		}


		//============================================
		//処理3.集計結果出力
		//============================================

		if(!outputResultFile("branch.out",args[0],officeMap,officeSalesSum)) {
			//返り値がfalseの場合、処理終了
			return;
		}
	}

	//============================================
	//処理1.支店定義ファイル読み込み(メソッド部)
	//============================================
	private static HashMap<String,String> inputMasterFile(String path,String inputFileName) {
		BufferedReader br = null;
		HashMap<String,String> masterMap = new HashMap<String,String>();
		try {
			//処理1-1.コマンドライン引数で指定されたディレクトリから支店定義ファイルを開く
			File officeDefinitionFile = new File(path + "\\" + inputFileName);

			//エラー処理1-1 支店定義ファイルが存在しない場合エラーメッセージを表示
			if(!officeDefinitionFile.exists()) {
				//エラーメッセージを表示し処理終了
				System.out.println("支店定義ファイルが存在しません");
				return null;
			}

			FileReader fr = new FileReader(officeDefinitionFile);
			br = new BufferedReader(fr);
			String content = "";


			//処理1-2.支店定義ファイルを一行ずつ読み込み、すべての支店コードとそれに対応する支店名を保持する
			//ファイル読み込み処理に必要な変数宣言
			String[] officeLineData = new String[2];
			while((content = br.readLine()) != null){
				//初期化
				officeLineData = new String[2];

				//「,」でスプリット
				officeLineData = content.split(",");

				//エラー処理1-2 ① 支店定義ファイルのフォーマットが不正な場合はエラーメッセージを表示
				if(officeLineData.length != 2) {
					//エラーメッセージを表示し処理終了
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return null;
				}
				//エラー処理1-2 ② 支店定義ファイルのフォーマットが不正な場合はエラーメッセージを表示
				if(!( officeLineData[0].matches("[0-9]{3}") )) {
					//エラーメッセージを表示し処理終了
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return null;
				}

				//フォーマットに問題がない場合、予め宣言したMap変数に退避
				masterMap.put(officeLineData[0], officeLineData[1]);

				//事前に支店の売り上げ合計額を0に設定
				//officeSalesSum.put(officeLineData[0],0L);
			}
			return masterMap;
		}catch(IOException e) {
			//処理1(支店定義ファイル読み込み中にExceptionが発生)
			//エラーメッセージを表示し処理終了
			System.out.println("予期せぬエラーが発生しました");
			return null;
		}finally {
			try {
				if(br != null) {
					br.close();
				}
			} catch (IOException e) {
				//finally処理で正常にcloseできなかった場合
				//エラーメッセージを表示し処理終了
				System.out.println("予期せぬエラーが発生しました");
			}
		}
	}

	//============================================
	//処理1.支店定義ファイル読み込み_集計ファイル初期値設定処理(メソッド部)
	//============================================
	private static HashMap<String,Long> calcMasterDefaultSet(Set<String> masterSet) {
		HashMap<String,Long> calcMasterMap = new HashMap<String,Long>();
		try {
			for(String master : masterSet) {
				//事前に支店の売り上げ合計額を0に設定
				calcMasterMap.put(master,0L);
			}
			return calcMasterMap;
		}catch(Exception e) {
			//処理1(支店定義ファイル読み込み中にExceptionが発生)
			//エラーメッセージを表示し処理終了
			System.out.println("予期せぬエラーが発生しました");
			return null;
		}
	}

	//============================================
	//処理3.集計結果出力(メソッド部)
	//============================================
	private static Boolean outputResultFile(String outputFileName,String path,HashMap<String,String> masterMap,HashMap<String,Long> calculateMap) {
		BufferedWriter bw = null;
		try {
			//処理3-1.コマンドライン引数で指定されたディレクトリに支店別集計ファイルを作成する。
			//		支店別集計ファイルのフォーマットに従い、全支店の支店コード、支店名、合計金額を出力する。
			FileWriter fw = new FileWriter(new File(path + "\\" + outputFileName));
			bw = new BufferedWriter(fw);

			//Mapキー値をリスト型に変換
			ArrayList<String> mapCodeList = new ArrayList<String>(masterMap.keySet());

			//支店情報ごとに計算された売り上げ合計金額を記録
			for(String masterCode : mapCodeList) {
				bw.write(masterCode + "," + masterMap.get(masterCode) + "," + calculateMap.get(masterCode) + "\r\n");
			}
			return true;
		}catch(IOException e) {
			//処理3(集計結果出力中にIOExceptionが発生)
			//エラーメッセージを表示し処理終了
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			try {
				if(bw != null) {
					bw.close();
				}
			} catch (IOException e) {
				//finally処理で正常にcloseできなかった場合
				//エラーメッセージを表示し処理終了
				System.out.println("予期せぬエラーが発生しました");
			}
		}
	}
}
